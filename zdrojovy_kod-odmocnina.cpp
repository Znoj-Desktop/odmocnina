/*
 * Soubor:  odmocnina.cpp
 * Datum:   2011/11/22
 * Autor:   Ji�� Znoj, zno0011
 * Projekt do ZP
 * Popis:   Program prov�d� v�po�et druh� odmocniny z celeho cisla. ��slo, z kter�ho se bude 
 *          odmocnina vypo��t�vat m��e obsahovat velk� mno�stv� cifer (i nad 10 tis�c).
 *          Vstupy: Program dostane na sv�j vstup libovoln� velk� ��slo, voliteln� pak jako dal�� parametr ��slo
 *					ud�vaj�c� p�esnost v�sledku - po�et desetinn�ch m�st v�sledn�ho ��sla.
 *			V�stupy: Program zobraz� do HTML souboru v�sledek v�po�tu druh� odmocniny.   
 * Pou�it�:	bez parametru			- zobrazen� n�pov�dy
 *			s parametrem "-h"		- zobrazen� n�pov�dy
 *			s parametrem "-help"	- zobrazen� n�pov�dy
 *			s 1 cislem				- vypocet odmocniny s presnosti 0
 *			se 2 cisly				- z prvniho cisla bude vypocitana odmocnina a 
 *									  druhe cislo udava presnost - pocet desetinnych mist vysledku
 *			s vice parametry		- chyba
 *			s "necisly"				- chyba
 *			se zapornymi cisly		- chyba
 * P��klad:	vstup:	./odmocnina.cpp 123456789 10
 *			v�stup:	vytvo�en� HTML souboru
 *					Odmocnina z 9-mistneho cisla 123465789 s presnosti na 10 desetinnych mist je:
 *					11111,1110605555
 *					Pokra�ujte stisknut�m libovoln� kl�vesy...
 *			vstup:	./odmocnina.cpp 123456789 1K
 *			v�stup:	Errorove hlaseni: Zadan chybny vstup.
 *					Pokra�ujte stisknut�m libovoln� kl�vesy...
 */

//pripojeni knihovny jazyka c++ pro vstup/vystupni operace
#include <iostream>
//knihovna je pouzita pro urceni aktualniho casu
#include <time.h>
using namespace std;

//vycet konstant k errorovym hlasenim
enum chyby
{
	EVSTUP = 0,	//chybny vstup
	ESOUBOR,	//chyba souboru
	EVYSTUP,	//chybny vystup
	EPROG,		//chyba za behu programu
	EMAX,		//prekroceno maximum
	ENEZNAMA	//neznama chyba
};

//texty ke konstantam - texty errorovych hlaseni
const char *ZPRAVA[] =
{
	//EVSTUP
	"Zadan chybny vstup.\r\n",
	//ESOUBOR
	"Chyba pri praci se souborem.\r\n",
	//EVYSTUP
	"Chybny vystup.\r\n",
	//EPROG
	"Chyba za behu programu.\r\n",
	//EMAX
	"Do tolika uz nenapocitam.\r\n",
	//ENEZNAMA
	"Neznama chyba.\r\n"
};


/** \brief vytiskne errorove hlaseni
 *
 * Vola se za ucelem vytisknuti prislusneho erroroveho hlaseni na standardni chybovy vystup z vyctu errorovych hlaseni. 
 * Program je pak ukoncen s chybovym navratovym kodem
 * @param error zkratka udavajici ktere errorove hlaseni se bude tisknout
 */
 void tiskchyby(int error)
{
	fprintf(stderr, "Errorove hlaseni: %s\r\n", ZPRAVA[error]);
	system("PAUSE");
	exit(EXIT_FAILURE);
} //konec void tiskchyby(int error)


/** \brief vytiskne napovedu
 *
 * Vytiskne napovedu na stadardni vystup.
 */
void napoveda()
{
	printf("Soubor: odmocnina.cpp\r\n");
	printf("Datum:  2011/11/22\r\n");
	printf("Autor:  Jiri Znoj, zno0011\r\n");
	printf("Projekt do ZP\r\n\r\n");

	printf("Popis:\r\n");
	printf("	Program provadi vypocet druhe odmocniny z celeho cisla.\r\n");
	printf("	Cislo, z ktereho se bude odmocnina vypocitavat\r\n");
	printf("	muze obsahovat velke mnozstvi cifer (i nad 10 tisic).\r\n");
	printf("	velke mnozstvi cifer (i nad 10 tisic).\r\n");
	printf("	Vstupy: Program dostane na svuj vstup libovolne velke cislo\r\n");
	printf("	a pak volitelne presnost - pocet desetinnych mist vysledku\r\n");
	printf("	Vystupy: Program zobrazi do HTML souboru vysledek\r\n");
	printf("	vypoctu druhe odmocniny.\r\n\r\n");

	printf("Pouziti:\r\n");
	printf("	bez parametru		- zobrazeni napovedy\r\n");
	printf("	s parametrem \"-h\"	- zobrazeni napovedy\r\n");
	printf("	s parametrem \"-help\"	- zobrazeni napovedy\r\n");
	printf("	s 1 cislem		- vypocet odmocniny s presnosti 0\r\n");
	printf("	se 2 cisly		- z prvniho cisla bude vypocitana odmocnina\r\n");
	printf("				  a druhe cislo udava presnost\r\n");
	printf("	s vice parametry	- chyba\r\n");
	printf("	s \"necisly\"		- chyba\r\n");
	printf("	se zapornymi cisly	- chyba\r\n");
	printf("	se zapornymi cisly	- chyba\r\n\r\n");

	printf("Priklady:\r\n");
	printf("	vstup:	./odmocnina cpp 123456789 10\r\n");
	printf("	vystup:	vytvoreni HTML souboru\r\n");
	printf("		Odmocnina z 9-mistneho cisla 123465789 s presnosti na 10 desetinnych mist je:\r\n");
	printf("		11111,1110605555\r\n");
	printf("		Pokracujte stisknutim libovolne klavesy...\r\n\r\n");

	printf("	vstup:	./odmocnina cpp 123456789 1K\r\n");
	printf("	vystup:	Errorove hlaseni: Zadan chybny vstup.\r\n");
	printf("		Pokracujte stisknutim libovolne klavesy...\r\n");

} //konec void napoveda()


/** \brief uvolnuje naalokovanou pamet
 *
 * Uvolnuje dynamicky naalokovanou pamet 2 poli
 * @param *polex ukazatel na dynamicky alokovanou pamet
 * @param *poley ukazatel na dynamicky alokovanou pamet
 */
void uklid(int *polex, int *poley)
{
	free(polex);
	free(poley);
} //konec void uklid(int *polex, int *poley)


/** \brief cte a kontroluje parametry prikazoveho radku
 *
 * Pokud neni zadan parametr, nebo pokud je zadan parametr "-h" nebo "-help", 
 * pak se vola funkce pro vypis napovedy a program uspesne skonci.
 * Poud je zadan 1 nebo 2 parametry, pak funkce nic nedela.
 * Pokud je zadan jiny nez drive uvedeny pocet parametru (3 a vice),
 * pak se vola funkce pro vytisknuti prislusneho erroroveho hlaseni
 *
 * @param argc udava pocet zadanych parametru se kterymi je program spusten
 * @param *argv[] je pole ukazatelu ukazujicich na pamet ve ktere je ulozen obsah parametru prikazoveho radku
 */
void kontrola_parametru(int argc, char *argv[])
{
	//pokud neni zadan zadny parametr, nebo prave jeden "-h" nebo prave jeden "-help", pak se vytiskne napoveda
	if((argc == 1) || ((argc == 2) && (strcmp("-h", argv[1]) == 0)) || ((argc == 2) && (strcmp("-help", argv[1]) == 0)))
	{
		napoveda();
		system("PAUSE");
		exit(EXIT_SUCCESS);
	}

	//vse je ok, program muze pokracovat
	else if (argc == 2 || argc == 3)
	{
		return;
	}

	//pokud je zadan jiny pocet parametru, pak se jedna o chybu
	else
	{
		tiskchyby(EVSTUP);
	}
} //konec void kontrola_parametru(int argc, char *argv[])


/** \brief kontroluje a zpracovava parametry
 *
 * Prevadi parametry prikazoveho radku na cislice, kontroluje jestli se jedna o cislice - pokud ano,
 * pak je v pripade prvniho parametru uklada v puvodnim poradi do pole zadaneho 3. parametrem funkce.
 * Jestlize je zadana i presnost (jestli je program spusten se 2 parametry), tak se zkontroluje
 * jestli se jedna o platne cislo (kontroluje se kazda cislice zvlast).
 * V pripade chyby (pokud je v parametrech jiny znak nez cislice), tak je volana funkce uklid a tiskchyby
 * @param argc udava pocet zadanych parametru se kterymi je program spusten
 * @param *argv[] je pole ukazatelu ukazujicich na pamet ve ktere je ulozen obsah parametru prikazoveho radku
 * @param *pole ukazatel na dynamicky alokovanou pamet do ktere se zapisi cisla 1. parametru prikazove radky
 * @param *pole2 ukazatel na dynamicky alokovanou pamet, pouziva se zde jen tehdy, kdyby bylo potreba ji uvolnit
 * @param pocet_znaku udava pocet znaku prvniho parametru prikazove radky
 */

void zpracovani_parametru(int argc, char *argv[], int *pole, int *pole2, const unsigned long long int pocet_znaku)
{
	int cislo = 0;
	char zbytek[2];
	//cyklus nacita cisla do promenne cislo a do pole zbytek nacita to, co cislo neni po jednotlivych cislicich
	for(int i = 0; i < pocet_znaku; i++)
	{
		sscanf(argv[1]+i, "%1d", &cislo);
		sscanf(argv[1]+i, "%c", zbytek);
		//pokud je na vstupu jiny znak nez cislo, pak je program ukoncen
		//uvolni se pamet a na stderr se vytiskne chybove hlaseni
		if(!isdigit(zbytek[0]))
		{
			uklid(pole, pole2);
			tiskchyby(EVSTUP);
		}
		pole[i] = cislo;
	} //konec for(int i = 0; i < MAX; i++)

	//jeli zadana i presnost, zkontrolujeme jestli se jedna o cislo
	if(argc == 3)
	{	
		//cylkus prochazi cele cislo po cislicich
		for(int i = 0; i < strlen(argv[2]); i++)
		{
			sscanf(argv[2]+i, "%c", zbytek);
			//pokud je na vstupu jiny znak nez cislo, pak je program ukoncen
			//uvolni se pamet a na stderr se vytiskne chybove hlaseni
			if(!isdigit(zbytek[0]))
			{

				uklid(pole, pole2);
				tiskchyby(EVSTUP);
			}
		}
	} //konec if(argc == 3)
} //konec void zpracovani_parametru(int argc, char *argv[], int *pole, int *pole2, const int pocet_znaku)


/** \brief umocnuje cislo
 *
 * Funkce pocita exponent-ou(2. parametr funkce) mocninu cisla zaklad(1. parametr funkce)
 * @param zaklad zaklad mocniny - mocnenec
 * @param exponent exponent - mocnitel
 * return exponent-ta mocnina cisla zaklad
 */
unsigned long long int mocnina(unsigned long long int zaklad, int exponent = 2)
{
	unsigned long long int pom = zaklad;

	if (exponent == 0)
	{
		return 1;
	}

	for (int i = 1; i < exponent; i++)
	{
		pom *= zaklad;
	}

	return pom;
} //konec unsigned long long int mocnina(unsigned long long int zaklad, int exponent = 2)


/** \brief prepise cislo do pole a vraci pocet cislic
 *
 * Cislo zadane v 1. parametru prepise po cislicich do pole zadaneho ve 2. parametru a vrati pocet cislic,
 * ze kterych je cislo slozene. Nejdriv se spocitaji cislice a pak se deli nasobkami deseti a postupne
 * se zapisuji vsechny cislice od nejvetsi do pole
 *
 * @param vstup cislo jehoz cislice chceme spocitat a ktere chceme prepsat do pole
 * @pole pole[] pole do nehoz chceme prepsat po cislicich cislo n tak, aby nejvyssi cifra byla na nulte pozici
 * @return pocet cislic ze kterych se sklada cislo predane v prvnim parametru funkce
 */
unsigned long int cislice(unsigned long long int vstup, int pole[])
{
	unsigned long long int pom = vstup;
	unsigned int pocitadlo = 0;
	unsigned int cisla = 0;

	//dokud je cislo vice nez 1 ciferne, pak se vydeli 10 a inkrementuje se promenna pocitadlo
	while (pom >= 10)
	{
		pocitadlo++;
		pom = pom/10;
	}

	//zapis prvni cislice z pomocne promenne pom
	pole[cisla] = pom;
	//do promenne n se ukladaji zbytky - coz je puvodni cislo ze ktereho je odebrano to, ktere je jiz zapsano v poli
	vstup = vstup%mocnina(10, pocitadlo);
	//to stejne se deje v cyklu, ktery se provadi tolikrat, kolik cislic ma prepisovane cislo
	for (int j = 0; j < pocitadlo;)
	{
		pocitadlo--;
		cisla++;
		pom = vstup/mocnina(10, pocitadlo);
		vstup = vstup%mocnina(10, pocitadlo);
		pole[cisla] = pom;
	}

	//navratova hodnota je pocet cislic
	return cisla+1;
} //konec unsigned long int cislice(unsigned long long int vstup, int pole[])


/** \brief vypocet odmocniny Newtonovou metodou
 *
 * Vypocita odmocninu z cisla zadaneho prvnim parametrem funkce s presnosti na tolik desetinnych mist, kolik
 * je zadano parametrem druhym. Pokud je funkce volana jen s jednim parametrem, pak je druhy parametr implicitne 1.
 * Funkci pouzivam pro odmocneni prvni dvojice cisel ve funkci odmocnina a to proto, protoze je potreba znat pouze
 * presne cislo bez desetinnych mist a na to staci obvykle jeden pruchod Newtonovou metodou davajici dostatecne
 * presny vysledek. Funkce pocita s typy "double" a vraci integer. Tim padem se vsechno za desetinnou carkou zahodi,
 * takze nedochazi k zaokrouhlovani. Tim padem vysledek "2,9" je vracen jako "2" - coz je zadouci.
 * Vzorec pro vypocet pomoci Newtonovy aproximacni metody je: x(k+1) = (1/2)*(x(k)+(a/x(k))), kde
 * k je prirozene cislo, a je cislo ze ktereho chceme odmocninu pocitat.
 *
 * @param a cislo ktere chceme odmocnit
 * @param presnost presnost kterou chceme pri vypoctu dosahnout
 * @return cela cast vysledku z 2. odmocniny cisla z prvniho parametru
 */
int odmocnina_nepresna (int a, unsigned int presnost = 1)
{
	if(a == 0)
	{
		return a;
	}
	//x(0) volime pro jednoduchost x(0) = a.
	double x = a;
	//promena dalsi je x+1. je vsak nutne uchovavat obe tyto pro menne pro dosazeni pozadovane presnosti
	double dalsi = a;
	double nova_presnost = 1;
	
	//prevedeni presnosti tak, aby bylo mozne porovnavat prubezne vysledky
	for(int i = 0; i < presnost; i++)
	{
		nova_presnost /= 10;
	}
	
	//cyklus pro vypocet odmocniny Newtonovou metodou
	do
	{
		x = dalsi;
		dalsi = 0.5*(x+(a/x));
	//Vypocet je ukoncen dosazenim zadane presnosti. Ta je implicitne 1 - coz je 1 platna pozice za desetinnou carkou
	}while ((x - dalsi) >= nova_presnost);
	//odriznuti desetinne casti vysledku, vracena hodnota je pouze cely integer
	return (double) dalsi;
} //konec int odmocnina_nepresna (int a, unsigned int accuracy_number = 1)

/** \brief vypocita odmocninu
 *
 * Odmocnina je pocitana pomoci alg. popsaneho podrobne v dokumentaci, cislo ze ktereho pocita odmocninu bere 
 * z pole1, ktere je zadano jako 1. parametr a take do nej uklada vysledek. Pro vypocet 1 cislice vysledku se 
 * pouziva dvojcisli a vysledek je cislice pouze jeda, aproto se cislo vysledkem neprepise. Usetri se tak pamet.
 * Vysledek skonci, az je dosazena bud zadana presnost, nebo pokud je jiz vysledek presny - pak je do zadane
 * presnosti zbyly pocet mist doplnen nulami.
 *
 * @param pole1[] v tomto poli je ulozeno cislo ktere budu odmocnivat a postupne sem ukladam vysledek
 * @param pole2[] toto pole se pouziva jako pomocne pro ruzne vypocty
 * @param cislic pocet cislic v cisle ze ktereho se vypocita odmocnina
 * @param presnost pocet platnych cisel vysledku za desetinnou carkou 
 */
void odmocnina(int pole1[], int pole2[], unsigned long int cislic, unsigned int presnost)
{
	unsigned long long int pom = 0;
	unsigned long long int pom_cyklus = 0;
	int cislic_v_pom = 0;
	short int vysledek = 0;
	int prvni = 0;
	int druhy = 1;
	bool konec_cyklu = 0;
	unsigned long long int mocnina_deseti = 1;

	//pokud je lichy pocet cislic, pak je na prvni pozici vzdy 0, kterou se pocet cislic doplni na sude cislo
	if(cislic%2)
	{
		pole2[0] = 0;
		pole2[1] = pole1[prvni];
		prvni +=1;
		druhy +=1;
	}

	else
	{
		//prvni++ se nejdrive pouzije a pak inkrementuje a tvori vzdy prvni cislici sepisovane dvojice
		//druhy++ se nejdrive pouzije a pak inkrementuje a tvori vzdy druhou cislici sepisovane dvojice
		pole2[0] = pole1[prvni];
		prvni +=2;
		pole2[1] = pole1[druhy];
		druhy +=2;
	}

	//zapis do vysledku - vzdy jen jedna cislice
	//prvni cislici ve vysledku tvori cela cast odmocniny nejlevejsi dvojice cisel z cisla ze ktereho odmocninu pocitame
	pole1[vysledek++] = odmocnina_nepresna(10*pole2[0]+pole2[1]);
	pom = ((10*pole2[0] + pole2[1]) - mocnina(pole1[0],2));

	//pokud odmocnina z pocitaneho cisla neni cele cislo mensi nez 100, pak se pokracuje nasledujicim cyklem
	if((cislic > 2) || (pom != 0))
	{
		//cyklu vypoctu 2. az posledni cislice vysledku------------------------------------------------------------------------------
		do
		{
			//pole2 znovu inicializujeme na -1, cislo -1 pak budeme pouzivat jako zarazku
			for(int i = 0; i < (cislic+presnost); i++)
			{
				pole2[i] = -1;
			}
			//urceni poctu cislic v promenne pom a prepis po cislicich do pole2
			cislic_v_pom = cislice(pom, pole2);
			//pokud tahle podminka nastane (nemela by, ale kdo vi), pak je prekroceno maximum co tento program dokaze spocitat
			if(cislic_v_pom < 0)
			{
				uklid(pole1, pole2);
				tiskchyby(EMAX);
			}
			//cislic_v_pom udava pocet cislic pocitanych od 1, ale pole je od 0 ->
			//-> proto je nasledujici pozice v poli rovna hodnote promenne "cislic_v_pom"
			pole2[cislic_v_pom] = pole1[prvni];
			//pokud vysledkem neni cele cislo, pak je potreba pocitat s cislem dale tak, jako by se na dalsich
			//pozicich delence nachazeli 0 (jsou tam ted na vsech pozicich -1)
			if(pole2[cislic_v_pom] < 0)
			{
				pole2[cislic_v_pom] = 0;
			}
			prvni +=2;

			pole2[cislic_v_pom+1] = pole1[druhy];
			
			//pokud vysledkem neni cele cislo, pak je potreba pocitat s cislem dale tak, jako by se na dalsich
			//pozicich delence nachazeli 0
			if(pole2[cislic_v_pom+1] < 0)
			{
				pole2[cislic_v_pom+1] = 0;
			}
			druhy +=2;
			
			//prizeni vsech potrebnych cislic do promenne pom
			pom *= 100;
			pom += 10*pole2[cislic_v_pom];
			pom_cyklus = 0;
			for(int i = 0; i < vysledek; i++)
			{
				pom_cyklus += mocnina(10,(vysledek-i-1))*pole1[i];
			}

			//zapis vysledku
			pole1[vysledek] = pom/(2*10*pom_cyklus);
			//nekdy se stane, ze je vysledek cislo vetsi nez 9 -> pak bude algoritmus "usmernen"
			//a do vysledku se priradi maximalni mozne jednociferne cislo - tedy 9 a budeme delat jako ze nic
			if(pole1[vysledek] > 9)
			{
				pole1[vysledek] = 9;
			}

			//spocitan rozdil hodnot, ke kteremu se nasledne opet pripise dvojice cisel z pocitane odmocniny
			pom_cyklus = 0;
			for(int i = 0; i < vysledek; i++)
			{
				pom_cyklus += pole1[i]*2*mocnina(10,(vysledek-i));
			}

			//dalsi chyba v algoritmu - pokud je podminka splnena, pak je vse ok, pokud ne, pak je ve vysledku
			//cislo vetsi nez by melo a je potreba jej dekrementovat
			if((pom + pole2[cislic_v_pom+1]) >= ((pom_cyklus+pole1[vysledek])*pole1[vysledek]))
			{
				pom = (pom + pole2[cislic_v_pom+1]) - ((pom_cyklus+pole1[vysledek])*pole1[vysledek]);	
			}
			
			else
			{
				//snizeni naposledy zapsaneho cisla do vysledku o 1
				pole1[vysledek]--;
				pom_cyklus = 0;
				for(int i = 0; i < vysledek; i++)
				{
					pom_cyklus += pole1[i]*2*mocnina(10,(vysledek-i));
				}
				pom = (pom + pole2[cislic_v_pom+1]) - ((pom_cyklus+pole1[vysledek])*pole1[vysledek]);
			}

			//podminka plati, pokud je odmocnina presne cislo a je zadana z vice nez 2 cislic
			if(((cislic/2) == vysledek) && (pom == 0))
			{
				vysledek++;
				//v cyklu jsou dosazeny 0 za desetinnou carkou tak, aby byl pocet desetinnych
				//mist stejny jako pocet cislic
				for(int i = 0; i < presnost; i++)
				{
					pole1[vysledek++] = 0;
				}
				//ted je uz cislo kompletni a muze se koncit
				konec_cyklu = 1;
			}
			
			//ukoncovaci podminka bude takova, ze pocet cislic ve vysledkuu bude ((pocet cislic v odmocnine) / 2) *coz
			//tvori celou cast cisla* + zadana presnost *coz je pocet cislic za desetinnou carkou*.
			if(((cislic/2) + presnost) <= vysledek)
			{
				konec_cyklu = 1;
			}
			vysledek++;
		}while(!konec_cyklu);
		//konec cyklu vypoctu 2. az posledni cislice vysledku-------------------------------------------------------------------------
		
	} //konec if((cislic > 2) || (pom != 0))

	//pokud je vysledek odmocniny ze zadaneho cisla mensiho nez 100 cele cislo, 
	//pak se do vysledku za desetinnou carku pricte pozadovany pocet 0 - podle zadane presnosti
	else
	{
		for(int i = 0; i < presnost; i++)
		{
			pole1[vysledek++] = 0;
		}
	}

	//zbytek pole je inicializovan na -1 *uz neni potreba kvuli zmene vypisu*
	for(int i = vysledek; i < cislic; i++)
	{
		pole1[i] = -1;
	}

} //konec void odmocnina(int pole1[], int pole2[], unsigned long int cislic, unsigned int presnost)

/** \brief vypis vysledku v jazyce html
 *
 * Zapise do souboru, jehoz odkaz je zadan prvnim parametrem se kterym je funkce volana, HTML kod, ve kterem bude
 * zadane cislo, zadana nebo implicitni presnost a vysledek. Vysledek se vypocita tak, ze pocet cislic v cele
 * casti vysledku je roven (v pripade licheho cisla vetsi polovine) poctu cislic v zadanem cisle. Pak se zapise
 * desetinna carka a za ni zbytek cisel, ktere se nachazi v poli ve kterem je vysledek ulozen. Pocet cislic za
 * desetinnou carkou je udavan presnosti. Dale je pak do HTML souboru vypsan cas ktery je v teto funkci vygenerovan.
 *
 * @param *soubor ukazatel na souvor do ktereho chceme HTML kod zapsat
 * @param *vstup ukazatel na cislo ze ktereho program pocita odmocninu. Je to ukazatel na 1. parametr prikazoveho radku
 * @param presnost pocet desetinnych mist kolik je spocitano - tedy i pocet desetinnych mist kolik bude mit vysledek
 * @param cislic_v_odmocnine udava pocet cislic zadaneho cisla 1. parametrem z prikazoveho radku
 * @param *pole ukazatel na pole ve kterem je ulozen vysledek
 */
void vypis (FILE *soubor, char *vstup, unsigned int presnost, unsigned long long int cislic_v_odmocnine, int *pole)
{	
	//pro vygenerovani aktualniho casu
	time_t cas;
	cas = time(NULL);

	fprintf(soubor, "<HTML>\r\n");
	fprintf(soubor, "<HEAD>\r\n");
	fprintf(soubor, "     <TITLE>ZP - Odomcnina; Ji�� Znoj - zno0011</TITLE>\r\n");
	fprintf(soubor, "</HEAD>\r\n");
	fprintf(soubor, "<BODY BGCOLOR=\"#0DB6FF\">\r\n");
	fprintf(soubor, "<CENTER> <H1> <u> ODMOCNINA </u> </H1>\r\n");
	
	fprintf(soubor, "ze zadan�ho ��sla ");
	fprintf(soubor, "<span style=\"color: #8000FF\">%s</span>", vstup);
	fprintf(soubor, " je s p�esnost� na ");
	fprintf(soubor, "<span style=\"color: #8000FF\">%d</span>", presnost);
	fprintf(soubor, " desetinn�ch m�st: ");
	fprintf(soubor, "<H2><span style=\"color: #0000FF\">");
	
	//desetinna carka se zavede za timto cyklem podle pravidla, ze pro odmocninu s x cislicemi plati:
	//odmocnina ma prave x/2 cislic - pro licha cisla se zaokrouhluje standardnim zpusobem (nahoru)
	for(int i = 0; i < (cislic_v_odmocnine/2 + cislic_v_odmocnine%2); i++)
	{
		fprintf(soubor, "%i", pole[i]);
	}
	//pokud je zadana presnost 0, pak zadna desetinna carka neni potreba
	if(presnost != 0)
	{
		fprintf(soubor, ",");
	}
	//cyklus se provede tolikrat, kolik platnych desetinnych mist je zadano/spocitano.
	for(int i = (cislic_v_odmocnine/2 + cislic_v_odmocnine%2); i < (cislic_v_odmocnine/2 + cislic_v_odmocnine%2 + presnost); i++)
	{
		fprintf(soubor, "%i", pole[i]);
	}

	fprintf(soubor, "</span></H2><br>\r\n");
	fprintf(soubor, "</CENTER>");
	fprintf(soubor, "<H6>tato str�nka je vygenerov�na programem <i>odmocnina.cpp</i> dne: %s <br>", ctime(&cas));
	
	fprintf(soubor, "<script>");
	fprintf(soubor, "var datum = new Date();\r\n");
	fprintf(soubor, "var denVTydnu = new Array(\"Sun\", \"Mon\", \"Tue\", \"Wed\", \"Thu\", \"Fri\", \"Sat\");\r\n");
	fprintf(soubor, "var retezec = \"pr�v� je \";\r\n");
	fprintf(soubor, "retezec += datum.getDate() + \" \";\r\n");
	fprintf(soubor, "retezec += denVTydnu[datum.getDay()] + \", \";\r\n");
	fprintf(soubor, "retezec += (1 + datum.getMonth()) + \" \";\r\n");
	fprintf(soubor, "document.write( retezec );\r\n");
	fprintf(soubor, "</script>\r\n");
	fprintf(soubor, "<span id=\"cas\"></span>\r\n");
	fprintf(soubor, "<script>\r\n");
	fprintf(soubor, "function naplnCas (){\r\n");
	fprintf(soubor, "var datum = new Date();\r\n");
	fprintf(soubor, "aktualniCas = datum.getHours() + \".\" + datum.getMinutes() + \":\" + datum.getSeconds();\r\n");
	fprintf(soubor, "window.document.getElementById(\"cas\").innerHTML = aktualniCas;\r\n");
	fprintf(soubor, "}\r\n");
	fprintf(soubor, "naplnCas();\r\n");
	fprintf(soubor, "window.setInterval(\"naplnCas()\", 1000)\r\n");
	fprintf(soubor, "</script>\r\n");
	fprintf(soubor, "<script>\r\n");
	fprintf(soubor, "retezec = datum.getFullYear() + \", \";\r\n");
	fprintf(soubor, "document.write( retezec );\r\n");
	fprintf(soubor, "</script>\r\n");
	fprintf(soubor, "<br>\r\n");
	fprintf(soubor, "Autor programu: Ji�� Znoj, zno0011\r\n");
	fprintf(soubor, "<center>\r\n");

	fprintf(soubor, "<a href=\"#\" onclick=\"window.close()\">zav��t str�nku</a>\r\n");
	fprintf(soubor, "</center>\r\n");
	fprintf(soubor, "</H6>\r\n");

	fprintf(soubor, "</BODY>\r\n");
	fprintf(soubor, "<HTML>\r\n");
} //konec void vypis (FILE *soubor, char *vstup, unsigned int presnost, unsigned long long int cislic_v_odmocnine, int *pole)


int main(int argc,char** argv)
{
	kontrola_parametru(argc, argv);
	const unsigned long long int MAX = strlen(argv[1]);
	unsigned long long int cislic_v_odmocnine = 0;
	unsigned int presnost = 0;

	//pokud podminka plati, tak je zadan parametr udavajici presnost a jeho hodnota se ulozi do prislusne promenne
	if(argc == 3)
	{
		presnost = atoi(argv[2]);
	}
	
	//dynamicka alokace poli
	//do pole "pole" se nejdrive nacte cislo ze vstupu, ze ktereho
	//ma byt pocitana odmocnina a na konci programu v nem bude vysledek.
	//pole je inicializovano na velikost o jedna vetsi nez je predpoklad vysledku,
	//protoze odmocnina z cisla o jedne cislici je zarovnana na sudy pocet cislic
	int *pole;
	pole = (int *) malloc (sizeof(int) * (MAX+presnost+1));
	//pole2 slouzi jako pomocne pole pro prubezne vypocty a je potreba jej mit o 1 vetsi nez
	//je velikost pole pro vstup/vysledek
	int *pole2;
	pole2 = (int *) malloc (sizeof(int) * (MAX+presnost+2));

	
	//obe pole inicializovany na -1 - tato hodnota slouzi jako zarazka, jelikoz
	//0 je platna cislice
	for(int i = 0; i < (MAX+presnost); i++)
	{
		pole[i] = -1;
		pole2[i] = -1;
	}

	//funkce, ktera spousti podprogram kontrolujici spravnost parametru zadanych z prikazoveho radku
	zpracovani_parametru(argc, argv, pole, pole2, MAX);

	cislic_v_odmocnine = MAX;

	//odmocnina nic nevraci, protoze vysledek bude v poli, ve kterem je zadane cislo
	//je to z duvodu setreni pameti
	odmocnina(pole, pole2, cislic_v_odmocnine, presnost);

	FILE *soubor;
	//otevreni souboru
	soubor = fopen("odmocnina.html", "w");

	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor == NULL)
	{
		tiskchyby(ESOUBOR);
	}

	//vypis do souboru HTML
	vypis(soubor, argv[1], presnost, cislic_v_odmocnine, pole);
	
	//vypis na standardni vystup (stdout)
	//-----------------------------------------------------------------------------------------------------------------
	//vypis odmocniny
	cout << "Odmocnina z "<< cislic_v_odmocnine << "-mistneho cisla " << argv[1] << " s presnosti na " << presnost << " desetinnych mist je: " << endl;
	//desetinna carka se zavede za timto cyklem podle pravidla, ze pro odmocninu s x cislicemi plati:
	//ze odmocnina ma prave x/2 cislic - pro licha cisla se zaokrouhluje standardnim zpusobem 
	for(int i = 0; i < (cislic_v_odmocnine/2 + cislic_v_odmocnine%2); i++)
	{
		cout << pole[i];
	}
	
	if(presnost != 0)
	{
		cout << ",";
	}
	
	//cyklus se provede tolikrat, kolik platnych desetinnych mist je spocitano.
	for(int i = (cislic_v_odmocnine/2 + cislic_v_odmocnine%2); i < (cislic_v_odmocnine/2 + cislic_v_odmocnine%2 + presnost); i++)
	{
		cout << pole[i];
	}

	cout << endl;
	//------------------------------------------------------------------------------------------------------------------
	//uzavreni souboru
	fclose(soubor);
	//uvoln�n� pole 
	uklid(pole, pole2);
	system("PAUSE");
	return 0;
	
}
