- [**Description**](#description)
- [**Technology**](#technology)
- [**Year**](#year)
- [**Úvod**](#uvod)
- [**Zadání**](#zadani)
  - [**Původní zadání**](#puvodni-zadani)
  - [**Mnou upravené zadání**](#mnou-upravene-zadani)
- [**Řešení**](#reseni)
  - [**Popis algoritmu**](#popis-algoritmu)
  - [**Ukázka algoritmu**](#ukazka-algoritmu)
- [**Implementace**](#implementace)
- [**Specifikace testů**](#specifikace-testu)
- [**Použití**](#pouziti)
  - [**Použité zdroje**](#pouzite-zdroje)
  - [**Metriky kódu**](#metriky-kodu)
- [**Screenshot**](#screenshot)
- [**Dokumentace**](#dokumentace)
- [**Programátorská dokumentace**](#programatorska-dokumentace)
- [**Prezentace**](#prezentace)


### **Description**
Program provede výpočet druhé odmocniny. Číslo, z kterého se bude odmocnina vypočítávat může obsahovat velké množství cifer (i nad 10 tisíc).  
Vstupy: Program dostane na svůj vstup libovolně velké číslo.  
Výstupy: Program zobrazí do HTML souboru výsledek výpočtu druhé odmocniny. Náležitě bude také zobrazen průběh výpočtu.  

---
### **Technology**
C++

---
### **Year**
2011

---
### **Úvod**  
Odmocňování v matematice je inverzní operací k umocňování, odmocnina je výsledkem této operace.  

Druhá odmocnina je speciálním typem obecné odmocniny. Často se označuje pouze jako odmocnina. Je-li definováno umocňování nějakých matematických objektů (čísel, matic, funkcí, …), pak druhá odmocnina z a, označovaná jako √a , je definována jako objekt b, pro který platí b^2 = a.  

Druhá odmocnina má také geometrický význam. √a je délka strany čtverce o obsahu S = a. Objev druhé odmocniny vedl ve starověku k objevení iracionálních čísel.  

V oboru reálných čísel uvedené obecné definici druhé odmocniny kladného čísla vyhovují dvě různá řešení, např. definici √4 vyhovují čísla 2 i – 2. Proto se obvykle odmocnina na množině reálných čísel bere jen jako kladné řešení a definuje se pouze pro kladná čísla, čímž se lze vyhnout problémům s existencí a jednoznačností odmocniny.  

Druhá odmocnina z čísel 1, 4, 9, 16... je celočíselná. Ve všech ostatních případech je rovna číslům iracionálním.

---
### **Zadání**  
#### **Původní zadání**  
Program provede výpočet druhé odmocniny. Číslo, z kterého se bude odmocnina vypočítávat může obsahovat velké množství cifer (i nad 10 tisíc).  

Vstupy: Program dostane na svůj vstup libovolně velké číslo.  

Výstupy: Program zobrazí do HTML souboru výsledek výpočtu druhé odmocniny. Náležitě bude také zobrazen průběh výpočtu.  

#### **Mnou upravené zadání**  
Program provádí výpočet druhé odmocniny z celého čísla. Číslo, z kterého se bude odmocnina vypočítávat může obsahovat velké množství cifer (i nad 10 tisíc).  

**Vstupy**  
Program dostane na svůj vstup libovolně velké číslo, volitelně pak jako další parametr číslo udávající přesnost výsledku - počet desetinných míst výsledného čísla.  

**Výstupy**  
Program zobrazí do HTML souboru výsledek výpočtu druhé odmocniny.  

**Použití**  
Bez parametru - zobrazení nápovědy  
s parametrem "-h" - zobrazení nápovědy  
s parametrem "-help" - zobrazení nápovědy  
s 1 číslem - výpočet odmocniny s přesností na 0 desetinných míst  
se 2 čísly - z prvního čísla bude vypočítaná odmocnina a
druhé číslo udává přesnost - počet desetinných míst výsledku  
s více parametry - chyba  
s "nečísly" - chyba  
se zápornými čísly - chyba  

**Příklad**  
_vstup_: ./odmocnina.cpp 123456789 10  
_výstup_: vytvoření HTML souboru  
Odmocnina z 9-mistneho cisla 123465789 s presnosti na 10 desetinnych mist je:  
11111,1110605555  
Pokračujte stisknutím libovolné klávesy…  

_vstup_: ./odmocnina.cpp 123456789 1K  
_výstup_: Errorove hlaseni: Zadan chybny vstup.  
Pokračujte stisknutím libovolné klávesy…  

---
### **Řešení**  
Rozhodl jsem se použít algoritmus který funguje následujícím způsobem  
#### **Popis algoritmu**  
1. odmocněnce rozdělíme na dvojčíslí zprava doleva  
2. první číslici odmocniny dostaneme odmocněním prvního dvojčíslí  
3. od prvního dvojčíslí odečteme druhou mocninu první číslice odmocniny a k rozdílu připíšeme další dvojčíslí  
   - pokud výsledkem odmocniny není celé číslo, pak dojde na situaci, že nebudeme mít co sepisovat. V takovém případě sepisujeme dvojčíslí 00 až dokud nebude platit ukončovací podmínka  
4. v dělenci zatrhneme poslední místo a zbylé číslo dělíme dvojnásobkem dosavadního výsledku,  
5. výsledek dělení zapíšeme do výsledku a současně ji připíšeme k děliteli  
6. vzniklé číslo násobíme stejným číslem a výsledek odečteme od původního čísla  

#### **Ukázka algoritmu**  
![](./README/alg.png)

---
### **Implementace**  
Číslo, které program dostane jako 1. parametr příkazového řádku je zpracováno tak, že program ve funkci kontroluje daný řetězec znak po znaku, jestli se skutečně jedná o číslice.  
Pokud ano, pak je ukládá do dynamicky alokovaného pole, které je alokováno na velikost počet znaků na vstupu + přesnost, která je zadaná jako 2. parametr příkazového řádku + 1 což je kvůli zarovnání lichého počtu číslic.
Přesnost udává počet desetinných míst, která budou spočítána a následně vypsána na výstup. Pokud po odmocnění vyjde celé číslo, pak je za desetinnou čárkou tolik nul, kolik udává přesnost.  
Algoritmus pro samotný výpočet využívá dvě dynamicky alokované pole – první, ve kterém je uložen vstup slouží také pro uložení výstupu, jelikož pro každé 2 zpracované číslice je právě jedna číslice výsledku. Výjimku tvoří číslice první pro číslo s lichým počtem číslic. Pro tuto číslici je totiž dvojicí 0, která není součástí čísla, ale pouze doplněk pro algoritmus, který pracuje se sudým počtem číslic v čísle. Druhé pole slouží pro ukládání velkých pomocných výpočtů, které by se mohly nevejít do některého datového typu pro relativně malá čísla. V případě, že je na vstupu jednociferná číslice, pak je potřeba pro pomocné výpočty pole o 1 větší – což je potřeba vyřešit při alokaci. Pole má tedy velikost |počet číslic na vstupu + přesnost + 2|.  

Samotný algoritmus je zpracován tak, že se vezme první dvojice čísel z pole a uloží se do proměnné. Levější číslice je samozřejmě násobena číslem 10, aby například 2 číslice v poli 2 a 5 tvořili po součtu skutečně 25 a ne 7. První dvojice je odmocněna funkcí nepřesná mocnina. Funkce je implementována Newtonovou aproximační metodou, kdy se pomocí vzorce `X[k+1] = (1/2 ).(X[k] + a/X[k])`, kde k je přirozené číslo a a je číslo ze kterého chceme odmocninu spočítat. Jelikož nás v algoritmu na výpočet odmocniny zajímá v případě odmocnění tohoto prvního dvouciferného čísla pouze celá část výsledku, tak je funkce s výpočtem odmocniny Newtonovou metodou zastavena jakmile je výsledek dostatečně přesný. Tato metoda se jeví jako nejrychlejší, jelikož nás zajímá jen velice nepřesné číslo, které dostaneme velice rychle.  
V algoritmu jsou ošetřeny obě dvě nalezené chyby, které jsou uvedeny v předchozím bodě a to přesně způsobem jakým je to vysvětleno.  
Každá číslice výsledku je uložena vždy na nejlevější pozici pole (na nejmenším indexu) kde ještě žádná číslice reprezentující výsledek není.  
S desetinnými čísly, které je nutné do výsledku zahrnout, se počítá, jako by to desetinná čísla nebyla.  
Ukončovací podmínka cyklu pro výpočet odmocniny dle vysvětleného algoritmu je dosažený počet číslic reprezentujících jednotlivá desetinná místa.  
Jelikož algoritmus s desetinnými čísly nepracuje, tak je spočítáno číslo ukončující algoritmus a to tak, že je počet číslic z čísla, ze kterého se odmocnina počítá vydělen dvěma a pokud se jedná o lichý počet, pak je číslo zaokrouhleno nahoru. K tomu číslu se přičte požadovaná přesnost a vyjde počet číslic, které je nutné algoritmem vypočítat.  

Výpis do souboru v jazyce HTML je realizován tak, že zadané číslo je vypsáno jako řetězec z prvního parametru příkazové řádky, přesnost jako proměnná a výsledek (zadané číslo po odmocnění) je vypsán ve dvou cyklech – v prvním je vypsána celá část výsledku, pak je vypsána desetinná čárka, za kterou následuje druhý cyklus, který vypisuje postupně číslici po číslici z pole, ve kterém je výsledek uložen – stejně jako v prvním cyklu pro výpis výsledku.  
Stejným způsobem je také realizován výpis výstupu na standardní výstup.  

Program očekává jeden nebo dva parametry pro spuštění programu. V případě jednoho to musí být kladné celé číslo, jinak se vypíše chyba na standardní chybový výstup a program skončí. Pokud jsou zadány dva parametry, pak to musí být čísla a platí pro obě stejné podmínky, jako když je zadán pouze jeden parametr.  

V případě chyb – například když jsou zadány více než 2 parametry, nebo parametry nevyhovující výše uvedeným podmínkám, pak je vyvoláno příslušné chybové hlášení na standardní chybový výstup, pokud je naalokovaná paměť, pak se uvolní, pokud je otevřen soubor, tak se uzavře a poté program skončí s chybovým návratovým kódem.  

---
### **Specifikace testů**  
**Testovací údaje č.: 1**  
Testovací kritérium: funkčnost výpočtu  
Testovací vstupy 123465789  
Očekávané výstupy: 11111  
Výsledek testování: 11111  
Poznámky: výsledek se shoduje s očekávaným výstupem  

**Testovací údaje č.: 2**  
Testovací kritérium: funkčnost výstupu se zadanou přesností  
Testovací vstupy 123456789 10  
Očekávané výstupy: 11111,1110605555  
Výsledek testování: 11111,1110605555  
Poznámky: výsledek se shoduje s očekávaným výstupem  

**Testovací údaje č.: 3**  
Testovací kritérium: odmocnina ze záporných čísel nemá řešení v R  
Testovací vstupy: -1  
Očekávané výstupy: chyba  
Výsledek testování: Errorove hlaseni: Zadan chybny vstup  
Poznámky: výsledek se shoduje s očekávaným výstupem  

**Testovací údaje č.: 4**  
Testovací kritérium: velké číslo  
Testovací vstupy: 13246578913246578913246578913246578913246578913246578913246578
913246578913246578913246578913246578913246578913246578913246578913246578913246578913246578913246578913246578913246578913246578913246  
Očekávané výstupy: 3.6395849918976447654881557330059740335286461076262488... × 10^96  
Výsledek testování: 3639584991897644760010910000504311194994122201010100118010001
141023250010010223202200239191091004  
Poznámky: na 19. pozici dojde ke zkreslení výsledku, což je způsobeno tím, že pro pomocné výpočty se používají proměnné typu unsigned long long int. Po konzultaci s vyučujícím jsem tam tuto nepřesnost zanechal. Řešením tohoto problému by bylo provádění veškerých výpočtů (sčítání, odčítání, násobení a umocňování) pomocí polí.  

**Testovací údaje č.: 5**
Testovací kritérium: Nesprávné zadaná přesnost  
Testovací vstupy 123 4a  
Očekávané výstupy: chyba  
Výsledek testování: Errorove hlaseni: Zadan chybny vstup  
Poznámky: výsledek se shoduje s očekávaným výstupem  

**Testovací údaje č.: 6**  
Testovací kritérium: Výpočet odmocniny pro číslo 0  
Testovací vstupy 0 20  
Očekávané výstupy: 0,00000000000000000000  
Výsledek testování: 0,00000000000000000000  
Poznámky: výsledek se shoduje s očekávaným výstupem  

---
### **Použití**  
Pro přeložení programu použijte Microsoft Visual Studio 2010, přeložení by mělo proběhnout bez chyb.  
Program se spustí příkazem ./odmocnina.cpp „parametr(y), který vybereme podle upraveného zadání“.  
Pro zobrazení nápovědy spusťte program s parametrem „-h“ (./odmocnina.cpp -h), „-help“ (./odmocnina.cpp -help), nebo bez parametrů (./odmocnina.cpp).  
Na standardní výstup je vypsána nápověda, zadaný vstup, zadaná přesnost a výstup, nebo v případě chyby nic.  
Na standardní chybový výstup se vypisují případná chybová hlášení.  

#### **Použité zdroje**  
http://cs.wikipedia.org/wiki/Druh%C3%A1_odmocnina
http://mfweb.wz.cz/matematika/2.htm
http://www.cplusplus.com

#### **Metriky kódu**  
Počet souborů: 1
Počet řádků zdrojového textu: 692
Velikost spustitelného souboru 50 688 B (Win7 64-bit, Visual studio 2040)

---
### **Screenshot**  
![](./README/odmocnina.png)

---
### **Dokumentace**  
[Dokumentace docx](./README.docx)  
[Dokumentace pdf](./README.pdf)  

---
### **Programátorská dokumentace**  
[Programátorská dokumentace doxygen](./README/Programatorska_dokumentace/index.html)  
[Doxyfile](./Doxyfile)  

---
### **Prezentace**  
[Prezentace pdf](./README/prezentace_odmocnina.pdf)  